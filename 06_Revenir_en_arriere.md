# Revenir en arrière

### **Situation** : vous avez changé votre CSS et plus rien ne fonctionne alors que tout marchait bien avant, que faire ?
**```code```**
* Avec la commande **```git log```**, vous pouvez avoir accès aux derniers *commits* que vous avez fait, naviguez dans la fenêtre avec "Entrée" et vous pouvez quitter avec "Q". La commande ```gitk``` vous donne aussi un aperçu assez lisible de vos *commits* mais ```git log``` permet de copier-coller simplement le code SHA du commit pour l'utiliser

![alt text](images/code_sha.png)

* Quittez ```git log``` et utilisez la commande **```git revert <code_sha>```** en remplaçant **<code_sha>** parce que vous avez pu récupérer avec `git log`

* Vos fichiers devraient être revenus à l'état dans lequel ils étaient lors du commit choisi